
var Question = Backbone.Model.extend({
    default: {
        text: '',
        choice1: '',
        choice2: '',
        choice3: ''
    }
});

var Questions = Backbone.Collection.extend({});

var question1 = new Question({
    text: 'How many days in a year?',
    choice1: '300',
    choice2: '366',
    choice3: '365'
});

var question2 = new Question({
    text: 'What is most populated country on Earth?',
    choice1: 'China',
    choice2: 'India',
    choice3: 'USA'
});

var questions = new Questions([question1, question2]);

var QuestionEditView = Backbone.View.extend({
    model: new Question(),
    el: $('.edit-panel'),
    initialize: function() {
        this.template = _.template($('.question-edit-template').html());
        this.render();
    },
    events: {
        'keyup': 'update'
    },
    update: _.debounce(
        function () {
            this.model.set({'text': $('#question').val(), 'choice1': $('#choice1').val(), 'choice2': $('#choice2').val(), 'choice3': $('#choice3').val()});
        },
        300
    ),
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));

        $('#save-button').on('click', function(e) {
            // console.log('save');
            var question = questionEditView.model;
            if (question.get('text') !== '' && question.get('choice1') !== '') {
                questions.add(question);

                $(e.target).addClass('hidden');
                $('#add-button').removeClass('hidden');
            }
        });

        return this;
    }
});



var questionEditView = new QuestionEditView({model: new Question({'text': '', 'choice1': '', 'choice2': '', 'choice3': ''})});

var QuestionView = Backbone.View.extend({
    model: new Question(),
    tagName: 'tr',
    initialize: function() {
        this.template = _.template($('.question-view-template').html());
        //Re-render our view when a model changes
        this.model.bind('change', _.bind(this.render, this));
    },
    events: {
        'click': 'edit'
    },
    edit: function(e) {
        questionEditView.model = this.model;
        questionEditView.render();

        $(e.target).parent().siblings().removeClass('active');
        $(e.target).parent().addClass('active');
        $('#save-button').addClass('hidden');
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});


var QuestionListView = Backbone.View.extend({
    model: questions,
    el: $('.question-list'),
    initialize: function() {
        var self = this;
        //Rerender if models added or removed
        this.model.bind('update', _.bind(this.render, this));

        this.render();
    },

    render: function() {
        var self = this;
        this.$el.html('');

        _.each(this.model.toArray(), function(question) {
            newQuestionView = new QuestionView({model: question});
            self.$el.append(newQuestionView.render().$el);
        });
        return this;
    }
});

var questionListView = new QuestionListView();



$('#add-button').on('click', function(e) {

    $('#save-button').removeClass('hidden');
    $(e.target).addClass('hidden');
    
    questionEditView.model = new Question({'text': '', 'choice1': '', 'choice2': '', 'choice3': ''});
    questionEditView.render();
});

